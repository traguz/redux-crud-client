import React,{Component} from 'react'

export default class Userlist extends Component{
    handleDelete(id){
        return () => 
            this.props.deleteUserFromDb(id)
    }
    render(){
        if(this.props.users.loading) return <div>Loading...</div>
        
        return (
            this.props.users.users.map(user => 
                <div key={user.name} style={{border:'1px solid black'}}>
                    <p>{user.name}</p>
                    <p>{user.surname}</p>
                    <p>{user.email}</p>
                    <p onClick={this.handleDelete(user['_id'])}> X </p>
                </div>
            )
        )
    }
} 