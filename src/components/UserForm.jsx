import React,{Component} from 'react'

export default class UserForm extends Component{
    handleSubmit=()=>{
        this.props.addUserToDb({
            name:this.refs.name.value,
            surname:this.refs.surname.value,
            email:this.refs.email.value
        })
        this.refs.name.value = ''
        this.refs.surname.value = ''
        this.refs.email.value = ''
    }

    render(){
        return(
            <div className='userForm'>
                <label>Name</label>
                <input type='text' placeholder='name' ref='name'/>
                <label>Surname</label>
                <input type='text' placeholder='surname' ref='surname'/>
                <label>Email</label>
                <input type='text' placeholder='email' ref='email'/>
                <button onClick={this.handleSubmit}>Submit</button>
            </div>
        )
    }
}
