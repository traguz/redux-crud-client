export const addUser = user => ({type: 'ADD_USER',user})
export const deleteUser = id => ({type: 'DELETE_USER',id})
export const requestUsers = () => ({type:'REQUEST_USERS'})
export const gotUsers = (users) => ({type:'GOT_USERS',users})

export function fetchUsers() {
    return dispatch => {
      dispatch(requestUsers())
      return fetch('http://localhost:8080/users/')
        .then(response => {
            console.log(response)
            return response.json()
        })
        .then(json => {
            console.log(json)
            dispatch(gotUsers(json))
        })
    }
}

export function addUserToDb(User){
    return dispatch => {
        return fetch('http://localhost:8080/users/',{
            method:'post',
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Content-Type': 'application/json'
            },
            body:JSON.stringify(User)
        })
        .then(response => response.json())
        .then(json=>dispatch(addUser(json)))
    }
}

export function deleteUserFromDb(id){
    return dispatch => {
           return fetch('http://localhost:8080/users/',{
            method:'delete',
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Content-Type': 'application/json'
            },
            body:JSON.stringify({id})
        })
        .then(response =>{
            if(response.status === 200){
                dispatch(deleteUser(id))
            }
        }) 
    }
}