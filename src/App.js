import React, { Component } from 'react';
import './app.css'

import UserFormR from './containers/UserFormR'
import UserListR from './containers/UserListR'
import GetButtonR from './containers/GetButtonR'

export default class App extends Component {

  render() {
    return (
      <div>
        <UserFormR/>
        <UserListR/>
        <GetButtonR/>
      </div>
    );
  }
}


