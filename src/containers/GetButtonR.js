import GetButton from '../components/GetButton'
import {connect} from 'react-redux'

import {fetchUsers} from '../actions'

export default connect(
    undefined,
    dispatch => ({fetchUsers: ()=>dispatch(fetchUsers())})
)(GetButton)
