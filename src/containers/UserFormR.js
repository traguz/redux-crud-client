import {connect} from 'react-redux'

import {addUserToDb} from '../actions'
import UserForm from '../components/UserForm'

const UserFormR =  connect(
    undefined,
    dispatch => ({
        addUserToDb: User => dispatch(addUserToDb(User))
    })
)(UserForm)

export default UserFormR