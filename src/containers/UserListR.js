import {connect} from 'react-redux'

import {deleteUserFromDb} from '../actions'

import UserList from '../components/UserList'

export default connect(
    state => ({users: state.users}),
    dispatch => ({deleteUserFromDb: (id)=>dispatch(deleteUserFromDb(id))})
)(UserList)

