export default (state={loading:false,users:[]},action)=>{
    switch(action.type){
        case 'ADD_USER':
            return {
                ...state,
                users:[
                    ...state.users,
                    action.user
            ]}
        case 'REQUEST_USERS':
            return{
                loading:true,
                users:state.users
            }
        case 'GOT_USERS':
            return {
                loading:false,
                users:action.users
            }
        case 'DELETE_USER':
            return{
               loading:false,
               users:state.users.filter(user => user['_id'] !== action.id)
            }
        default:
            return state
    }
}